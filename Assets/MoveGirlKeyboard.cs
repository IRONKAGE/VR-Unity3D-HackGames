﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveGirlKeyboard : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }
    
	public float speed = 150;
    // Update is called once per frame
    void Update()
    {
        Vector3 pos = transform.position;
 
         if (Input.GetKey ("w")) {
             pos.z += speed * Time.deltaTime;
         }
         if (Input.GetKey ("s")) {
             pos.z -= speed * Time.deltaTime;
         }
         if (Input.GetKey ("d")) {
             pos.x += speed * Time.deltaTime;
         }
         if (Input.GetKey ("a")) {
             pos.x -= speed * Time.deltaTime;
         }
         if (Input.GetKey ("q")) {
             pos.y += speed * Time.deltaTime;
         }
         if (Input.GetKey ("e")) {
             pos.y -= speed * Time.deltaTime;
         }
		 if (Input.GetMouseButtonDown(0)) {			

				
			}
 
         transform.position = pos;
    }
}
